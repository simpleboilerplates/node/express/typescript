import { Request, Response, Router } from 'express';


class ExampleController {
    private static async example(req: Request, res: Response) {
        res.send('Hello World !');
    }

    static registerRoutes() {
        const router = Router();
        router.get('/example', this.example);
        return router;
    }
}

export default ExampleController.registerRoutes();
