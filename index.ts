import { Application } from './src/application';

Application.bootstrap();
Application.run();