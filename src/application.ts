import bodyParser from 'body-parser';
import cors from 'cors';
import express, { Router } from 'express';
import http from 'http';
import { Configuration } from './configuration';
import ExampleController from './controllers/example.controller';

export class Application {
    private static app: express.Application;
    private static http: http.Server;

    static bootstrap() {
        this.app = express();
        this.http = new http.Server(this.app);
        this.registerMiddlewares();
        this.registerRoutes();
    }

    // Register the middlewares of the application
    static registerMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors());
    }

    // Register the routes of the application
    static registerRoutes() {
        const router = Router();
        router.use('/api', ExampleController)
        this.app.use(router);
    }

    static run() {
        this.http.listen(Configuration.APP_PORT, Configuration.APP_HOST, () => {
            console.log(`Listening on ${Configuration.APP_HOST}:${Configuration.APP_PORT}...`)
        });
    }
}
